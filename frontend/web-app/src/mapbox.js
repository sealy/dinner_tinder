import mapbox from 'mapbox-gl';

// https://docs.mapbox.com/help/glossary/access-token/
mapbox.accessToken = 'pk.eyJ1Ijoic2VhbHkiLCJhIjoiY2w0cGpyNXl6MGF5dTNkczJrbDUxYzVtdiJ9.KA5vZVoHMrPyydimuL2ssw';

const key = Symbol();

export { mapbox, key };